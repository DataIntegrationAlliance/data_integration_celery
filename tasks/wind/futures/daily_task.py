#! /usr/bin/env python3
"""
@author  : MG
@Time    : 2021/10/12 10:33
@File    : daily_task.py
@contact : mmmaaaggg@163.com
@desc    : 用于
"""
import logging

from tasks.wind import import_future_daily, daily_to_model_server_db
from tasks.wind.future_reorg.reversion_rights_factor import task_save_adj_factor
from tasks.wind.futures.future_mid_day_task import run_mid_day_task
from tasks.wind.to_vnpy import reversion_rights_factors_2_vnpy, wind_future_daily_2_vnpy

logger = logging.getLogger()


def _run_task():
    # DEBUG = True
    wind_code_set = None
    # import_future_info_hk(chain_param=None)
    # update_future_info_hk(chain_param=None)
    # import_future_info(chain_param=None)
    # # 导入期货每日行情数据
    import_future_daily(None, wind_code_set)
    # 根据商品类型将对应日线数据插入到 vnpy dbbardata 表中
    wind_future_daily_2_vnpy(start_vnp=True)
    # 部分合约的分钟就数据导入及迁移到 vnpy 数据库（午盘任务）：
    # 为提高数据导入效率，减少分钟数据导入时间，每日收盘后，仅优先将部分合约的分钟数据导入的库中
    run_mid_day_task()
    # 重新计算复权数据
    task_save_adj_factor()
    reversion_rights_factors_2_vnpy()

    # 按品种合约倒叙加载每日行情
    # load_by_wind_code_desc(instrument_types=[
    #     ('RB', r"SHF"),
    #     ('I', r"DCE"),
    #     ('HC', r"SHF"),
    # ])
    logger.info("all task finished")


def run_daily_only(start_vnp=True):
    wind_code_set = None
    # 导入期货每日行情数据
    import_future_daily(None, wind_code_set)
    # 根据商品类型将对应日线数据插入到 vnpy dbbardata 表中
    wind_future_daily_2_vnpy(start_vnp=start_vnp)
    # 重新计算复权数据
    task_save_adj_factor()
    reversion_rights_factors_2_vnpy()


def run_daily_to_model_server_db():
    daily_to_model_server_db(instrument_types=['rb', 'hc', 'i', 'j', 'jm'])


if __name__ == "__main__":
    # import_future_info(None)
    # run_daily_only(start_vnp=True)
    # _test_get_main_secondary_contract_by_instrument_types()
    _run_task()
